package com.k2.notification.controller;

import org.atmosphere.config.service.Disconnect;
import org.atmosphere.config.service.ManagedService;
import org.atmosphere.config.service.Ready;
import org.atmosphere.cpr.AtmosphereResource;
import org.atmosphere.cpr.AtmosphereResourceEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by abhijit.patil on 12/5/2017.
 */
@ManagedService(path = "/notifications")
public class NotificationManager {

    private Logger logger = LoggerFactory.getLogger(NotificationManager.class);
    private Map<String, Timer> timerMap = new HashMap<>();


    @Ready
    public void onReady(final AtmosphereResource resource) {
        logger.info("Connected: {}", resource.uuid());
        DataSender dataSender = new DataSender(resource);
        Timer timer = new Timer(true);
        timer.scheduleAtFixedRate(dataSender, 0, 5000);
        timerMap.put(resource.uuid(), timer);
    }

    @Disconnect
    public void onDisconnect(AtmosphereResourceEvent event) {
        logger.info("Disconnected: {}", event.getResource().uuid());
        cleanUp(event.getResource().uuid());
    }

    private void cleanUp(String uuid) {
        Timer timer = timerMap.get(uuid);
        if (timer != null) {
            logger.info("Removing timer task");
            timer.cancel();
            timerMap.remove(uuid);
        }
    }


    private class DataSender extends TimerTask {

        private final AtmosphereResource atmosphereResource;
        private int number;

        public DataSender(AtmosphereResource atmosphereResource) {
            this.atmosphereResource = atmosphereResource;
        }

        @Override
        public void run() {
            number++;
            logger.info("Sending number to connected resource: {}", number);
            atmosphereResource.getResponse().write(String.valueOf(number));
        }
    }


}
