var subSocket;
function initSocket() {
	"use strict";
	var socket = atmosphere;
	var request = {
		url: "http://172.104.165.90:9000/notifications",
		contentType: "application/json",
		transport: 'websocket'
	};

	request.onOpen = function (response) {
		document.getElementById("randomNumber").innerText = "Connected";
	};

	request.onMessage = function (response) {
		document.getElementById("randomNumber").innerText = "Number: " + response.messages[0];
	};

	request.onClose = function (response) {
		document.getElementById("randomNumber").innerText = "Disconnected";
	}

	request.onError = function (response) {
	};

	subSocket = socket.subscribe(request);

}
